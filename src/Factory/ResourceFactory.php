<?php

namespace Tz7\EveSwaggerClient\Factory;


use Tz7\EveSwaggerClient\ClientInterface;
use Tz7\EveSwaggerClient\Resource\AllianceResource;
use Tz7\EveSwaggerClient\Resource\CharacterResource;
use Tz7\EveSwaggerClient\Resource\CorporationResource;
use Tz7\EveSwaggerClient\Resource\SearchResource;
use Tz7\EveSwaggerClient\Resource\UniverseResource;


class ResourceFactory
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return AllianceResource
     */
    public function createAllianceResource()
    {
        return new AllianceResource($this->client);
    }

    /**
     * @return CharacterResource
     */
    public function createCharacterResource()
    {
        return new CharacterResource($this->client);
    }

    /**
     * @return CorporationResource
     */
    public function createCorporationResource()
    {
        return new CorporationResource($this->client);
    }

    /**
     * @return SearchResource
     */
    public function createSearchResource()
    {
        return new SearchResource($this->client);
    }

    /**
     * @return UniverseResource
     */
    public function createUniverseResource()
    {
        return new UniverseResource($this->client);
    }
}
