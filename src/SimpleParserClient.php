<?php

namespace Tz7\EveSwaggerClient;


use GuzzleHttp;
use Tz7\EveSwaggerClient\Loader\DocumentLoader;
use Tz7\EveSwaggerClient\Loader\DocumentLoaderInterface;
use Tz7\EveSwaggerClient\Validator\DocumentValidator;


/**
 * Adapter for SwaggerClient
 */
class SimpleParserClient implements ClientInterface
{
    /** @var SwaggerClient */
    private $swaggerClient;

    /**
     * @param array                        $config
     * @param DocumentLoaderInterface|null $documentLoader
     */
    public function __construct(array $config, DocumentLoaderInterface $documentLoader = null)
    {
        // Backward Compatibility TODO Remove with 0.1.0
        if ($documentLoader === null)
        {
            $documentLoader = new DocumentLoader(new DocumentValidator());

            trigger_error('Optional DocumentLoader will be removed with 0.1.0', E_USER_DEPRECATED);
        }

        $this->swaggerClient = new SwaggerClient($config, $documentLoader);
    }

    /**
     * @param string $method
     * @param string $route
     * @param array  $parameters
     *
     * @return array
     */
    public function request($method, $route, array $parameters = [])
    {
        return $this->parseResponse((string) $this->swaggerClient->request($method, $route, $parameters)->getBody());
    }

    /**
     * @param string $responseBody
     *
     * @return array
     */
    private function parseResponse($responseBody)
    {
        return GuzzleHttp\json_decode($responseBody, true);
    }
}
