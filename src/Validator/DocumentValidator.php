<?php

namespace Tz7\EveSwaggerClient\Validator;

use InvalidArgumentException;

class DocumentValidator
{
    /**
     * @param array $document
     *
     * @throws InvalidArgumentException
     */
    public function validate(array $document)
    {
        if (!isset($document['swagger'], $document['info'], $document['paths']))
        {
            throw new InvalidArgumentException('Swagger document is missing required sections.');
        }
    }
}
