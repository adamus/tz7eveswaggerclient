<?php

namespace Tz7\EveSwaggerClient\Loader;


use function GuzzleHttp\json_decode;
use Tz7\EveSwaggerClient\Validator\DocumentValidator;


class DocumentLoader implements DocumentLoaderInterface
{
    /** @var DocumentValidator */
    private $documentValidator;

    /**
     * @param DocumentValidator $documentValidator
     */
    public function __construct(DocumentValidator $documentValidator)
    {
        $this->documentValidator = $documentValidator;
    }

    /**
     * @inheritdoc
     */
    public function load($url)
    {
        $document = json_decode(file_get_contents($url), true);

        $this->documentValidator->validate($document);

        return $document;
    }
}
