<?php

namespace Tz7\EveSwaggerClient\Loader;


interface DocumentLoaderInterface
{
    /**
     * @param string $url
     *
     * @return array
     */
    public function load($url);
}
