<?php

namespace Tz7\EveSwaggerClient\Resource;


use Tz7\EveSwaggerClient\ClientInterface;


class CorporationResource
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $corporationId
     *
     * @return array
     */
    public function getById($corporationId)
    {
        return $this->client->request(
            ClientInterface::GET,
            '/corporations/{corporation_id}/',
            [
                'corporation_id' => $corporationId
            ]
        );
    }

    /**
     * @param int $corporationId
     *
     * @return array
     */
    public function getAllianceHistoryById($corporationId)
    {
        return $this->client->request(
            ClientInterface::GET,
            '/corporations/{corporation_id}/alliancehistory/',
            [
                'corporation_id' => $corporationId
            ]
        );
    }
}
