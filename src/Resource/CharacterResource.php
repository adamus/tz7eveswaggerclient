<?php

namespace Tz7\EveSwaggerClient\Resource;


use Tz7\EveSwaggerClient\ClientInterface;


class CharacterResource
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $characterId
     *
     * @return array
     */
    public function getById($characterId)
    {
        return $this->client->request(
            ClientInterface::GET,
            '/characters/{character_id}/',
            [
                'character_id' => $characterId
            ]
        );
    }

    /**
     * @param int $characterId
     *
     * @return array
     */
    public function getCorporationHistoryById($characterId)
    {
        return $this->client->request(
            ClientInterface::GET,
            '/characters/{character_id}/corporationhistory/',
            [
                'character_id' => $characterId
            ]
        );
    }
}
