<?php

namespace Tz7\EveSwaggerClient\Resource;


use Tz7\EveSwaggerClient\ClientInterface;


class UniverseResource
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getNamesByIds(array $ids)
    {
        return $this->client->request(
            ClientInterface::POST,
            '/universe/names/',
            [
                'ids' => $ids
            ]
        );
    }
}
