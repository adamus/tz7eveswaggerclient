<?php

namespace Tz7\EveSwaggerClient\Resource;


use Tz7\EveSwaggerClient\ClientInterface;


class SearchResource
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $search
     * @param array  $categories
     * @param bool   $strict
     *
     * @return array
     */
    public function getIdsByName($search, array $categories, $strict)
    {
        return $this->client->request(
            ClientInterface::GET,
            '/search/',
            [
                'search'     => $search,
                'categories' => implode(',', $categories),
                'strict'     => $strict ? 'true' : 'false'
            ]
        );
    }
}
