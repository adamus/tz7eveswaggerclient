<?php

namespace Tz7\EveSwaggerClient\Resource;


use Tz7\EveSwaggerClient\ClientInterface;


class AllianceResource
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $allianceId
     *
     * @return array
     */
    public function getById($allianceId)
    {
        return $this->client->request(
            ClientInterface::GET,
            '/alliances/{alliance_id}/',
            [
                'alliance_id' => $allianceId
            ]
        );
    }
}
