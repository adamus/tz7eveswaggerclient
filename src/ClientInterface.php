<?php

namespace Tz7\EveSwaggerClient;


interface ClientInterface
{
    const GET    = 'get';
    const POST   = 'post';
    const PUT    = 'put';
    const DELETE = 'delete';

    /**
     * @param string $method
     * @param string $route
     * @param array  $parameters
     *
     * @return array
     */
    public function request($method, $route, array $parameters = []);
}
