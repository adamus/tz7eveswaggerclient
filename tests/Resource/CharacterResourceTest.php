<?php

namespace Tz7\EveSwaggerClient\Test\Resource;


use Tz7\EveSwaggerClient\Resource\CharacterResource;


class CharacterResourceTest extends AbstractResourceTest
{
    /** @var CharacterResource */
    protected $resource;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->resource = new CharacterResource($this->client);
    }

    public function testGetCharacterById()
    {
        $result = $this->resource->getById(999856849);

        $this->assertEquals('Adamus TorK', $result['name']);
    }

    public function testGetCorporationHistoryById()
    {
        $result = $this->resource->getCorporationHistoryById(999856849);

        $records = array_column($result, 'record_id');

        $this->assertContains(6286374, $records);
    }
}
