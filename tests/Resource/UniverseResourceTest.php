<?php

namespace Tz7\EveSwaggerClient\Test\Resource;


use Tz7\EveSwaggerClient\Resource\UniverseResource;


class UniverseResourceTest extends AbstractResourceTest
{
    /** @var UniverseResource */
    protected $resource;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->resource = new UniverseResource($this->client);
    }

    public function testGetNamesByIds()
    {
        $result = $this->resource->getNamesByIds([30000142, 95465499]);

        $names = array_column($result, 'name');

        $this->assertContains('Jita', $names);
        $this->assertContains('CCP Bartender', $names);
    }
}
