<?php

namespace Tz7\EveSwaggerClient\Test\Resource;


use Tz7\EveSwaggerClient\Resource\AllianceResource;


class AllianceResourceTest extends AbstractResourceTest
{
    /** @var AllianceResource */
    protected $resource;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->resource = new AllianceResource($this->client);
    }

    public function testGetAllianceById()
    {
        $result = $this->resource->getById(1399057309);

        $this->assertEquals('HUN Reloaded', $result['name']);
        $this->assertEquals('RAX', $result['ticker']);
        $this->assertEquals(545096348, $result['executor_corporation_id']);
    }
}
