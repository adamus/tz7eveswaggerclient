<?php

namespace Tz7\EveSwaggerClient\Test\Resource;


use Tz7\EveSwaggerClient\SimpleParserClient;
use PHPUnit_Framework_TestCase;


abstract class AbstractResourceTest extends PHPUnit_Framework_TestCase
{
    /** @var SimpleParserClient */
    protected $client;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $config = [
            'scheme'     => 'https',
            'swagger'    => 'https://esi.tech.ccp.is/latest/swagger.json?datasource=tranquility',
            'headers' => [
                'User-Agent' => 'Testing Tz7\EveSwaggerClient'
            ]
        ];

        $this->client = new SimpleParserClient($config);
    }
}
