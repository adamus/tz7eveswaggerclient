<?php

namespace Tz7\EveSwaggerClient\Test\Resource;


use Tz7\EveSwaggerClient\Resource\CorporationResource;


class CorporationResourceTest extends AbstractResourceTest
{
    /** @var CorporationResource */
    protected $resource;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->resource = new CorporationResource($this->client);
    }

    public function testGetCorporationById()
    {
        $result = $this->resource->getById(545096348);

        $this->assertEquals('HUN Corp.', $result['name']);
        $this->assertEquals('HUN', $result['ticker']);
        $this->assertEquals(1399057309, $result['alliance_id']);
        $this->assertNotEmpty($result['description']);
    }

    public function testGetCorporationHistoryById()
    {
        $result = $this->resource->getAllianceHistoryById(545096348);

        $records = array_column($result, 'record_id');

        $this->assertContains(92942, $records);
    }
}
