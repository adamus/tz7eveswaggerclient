<?php

namespace Tz7\EveSwaggerClient\Test\Resource;


use Tz7\EveSwaggerClient\Resource\SearchResource;


class SearchResourceTest extends AbstractResourceTest
{
    /** @var SearchResource */
    protected $resource;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->resource = new SearchResource($this->client);
    }

    public function testSearchIdsByName()
    {
        $result = $this->resource->getIdsByName('Territorial Claim Unit', ['inventory_type'], true);

        $this->assertArrayHasKey('inventory_type', $result);
        $this->assertContains(32226, $result['inventory_type']);
    }
}
