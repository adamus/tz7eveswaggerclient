<?php

namespace Tz7\EveSwaggerClient\Test\Factory;

use PHPUnit_Framework_TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Tz7\EveSwaggerClient\ClientInterface;
use Tz7\EveSwaggerClient\Factory\ResourceFactory;
use Tz7\EveSwaggerClient\Resource\AllianceResource;
use Tz7\EveSwaggerClient\Resource\CharacterResource;
use Tz7\EveSwaggerClient\Resource\CorporationResource;
use Tz7\EveSwaggerClient\Resource\SearchResource;
use Tz7\EveSwaggerClient\Resource\UniverseResource;


class ResourceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ResourceFactory */
    private $resourceFactory;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        /** @var ClientInterface|PHPUnit_Framework_MockObject_MockObject $clientMock */
        $clientMock = $this->getMockBuilder(ClientInterface::class)->getMock();

        $this->resourceFactory = new ResourceFactory($clientMock);
    }

    public function testAllianceResourceFactoring()
    {
        $this->assertInstanceOf(AllianceResource::class, $this->resourceFactory->createAllianceResource());
    }

    public function testCharacterResourceFactoring()
    {
        $this->assertInstanceOf(CharacterResource::class, $this->resourceFactory->createCharacterResource());
    }

    public function testCorporationResourceFactoring()
    {
        $this->assertInstanceOf(CorporationResource::class, $this->resourceFactory->createCorporationResource());
    }

    public function testSearchResourceFactoring()
    {
        $this->assertInstanceOf(SearchResource::class, $this->resourceFactory->createSearchResource());
    }

    public function testUniverseResourceFactoring()
    {
        $this->assertInstanceOf(UniverseResource::class, $this->resourceFactory->createUniverseResource());
    }
}
